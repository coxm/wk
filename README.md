# wk
Allows creation of shell environments with context-specific settings. It runs a
setup script on entering an environment, and a teardown script on leaving the
environment.


## Installation
**wk** requires an environment variable `WK_HOME`. This can be set, for
example, in your `.bashrc` before sourcing the script:

    export WK_HOME=$HOME/wk_home
    source /path/to/wk.sh


## Documentation
Documentation can be found in `/doc/md/wk.md`. Manpages can be built as
follows (requires `pandoc`).

    make docs
    make install

The latter installs the manpages somewhere `man` can find them, and may require
`sudo`.


## Syntax

    # Run a sub-command.
    wk <command> [options] <project-name>

    # Start using an environment (shorthand for `wk setup # <project>`).
    wk <project-name>


## Basic env usage
To create an env, use the `wk new` command:

    wk new <dir> [name]

This creates a new environment with `dir` as its root directory. If `name` is
omitted, it defaults to `basename "$dir"`.

The env can then be entered using `wk setup <name>`, or just `wk <name>` for
short. Exit the env using `wk teardown <name>`.


## Super-envs
A super-env is an env which can contain child envs. This can be useful for
grouping sub-projects together under a master project, especially if the
sub-projects share a lot of common functionality.

When creating a super-env, a new function is defined in the super-env's setup
script. This function has the name provided and manages only the super-env's
children.

### Syntax

    wk super <existing> <local-wk-function>

### Example

    wk new ~/parent_dir parent

    # Create a local wk function:
    wk super parent pwk

    # Create a sup-env, using the local wk function:
    pwk new ~/parent_dir/child_dir child

    # Work on child.
    pwk child

    # Use wk as usual, creating a global env:
    wk new ~/global global

## LICENSE
(See `LICENSE`.)

Permission is granted to copy, distribute and/or modify this document under the
terms of the GNU General Public License, version 3 or any later version
published by the Free Software Foundation.
