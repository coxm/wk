#!/bin/bash

# set -o errexit
set -o nounset
set -o pipefail
if [[ "${TRACE-0}" == "1" ]]; then
    set -o xtrace
fi


# Copyright 2013, Michael Cox

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# wk [options] <project>
# wk [options] <command> [command-options] <command-arguments>


# WK_SUCCESS=0
WK_NO_SUCH_COMMAND=1
WK_NO_SUCH_OPTION=2
WK_NO_SUCH_SCRIPT=4
WK_NO_SUCH_ENV=8
WK_INVALID_SYNTAX=16
WK_INVALID_ARGUMENT=32
WK_ENV_ALREADY_EXISTS=64
WK_HOME_IS_NOT_SET=128


wk_util_print() {
    echo -e "[wk] $*"
}

wk_util_debug() {
    if [[ -n "${WK_VERBOSE:-}" ]]; then
        echo "$@"
    fi
}


if [[ -z "$WK_HOME" ]]; then
  if [[ -z "$PROJECTSETUP_HOME" ]]; then
    >&2 echo "Error: WK_HOME is not set"
    exit $WK_HOME_IS_NOT_SET
  fi

  echo "Warning: PROJECTSETUP_HOME is deprecated in favour of WK_HOME"
  WK_HOME="$PROJECTSETUP_HOME"
fi


# Call a script belonging to a project.
#
#   wk_util_callscript <project> [script=setup]
#
# Notice the difference in argument order between this function and
# wk_util_callscript_recurse. This allows wk_util_callscript to default to
# running the setup script.
wk_util_callscript() {
    local project_id="${1:-}"
    if [[ -z "$project_id" ]]; then
        >&2 echo "No project specified"
        return "$WK_INVALID_ARGUMENT"
    fi

    local command="${2:-setup}"
    local directory="$WK_HOME/$project_id"
    if ! [ -d "$directory" ]; then
        >&2 echo "Directory '$directory' doesn't exist"
        return "$WK_NO_SUCH_ENV"
    fi

    local script="$directory/$command.sh"
    if ! [[ -f "$script" ]]; then
        >&2 echo "Script '$script' doesn't exist"
        return "$WK_NO_SUCH_SCRIPT"
    fi
    # shellcheck source=/dev/null
    source "$script" "$@"
}


# Call a script multiple times, descending into to an ancestor project.
#
#   wk_util_callscript_recurse <script> [project...]
#
# Notice the difference in argument order between this function and
# wk_util_callscript.
wk_util_callscript_recurse() {
    local current_home="${WK_HOME:-}"
    if [[ -z "$current_home" ]]; then
        >&2 echo "Error: WK_HOME is unset"
        return "$WK_HOME_IS_NOT_SET"
    fi

    local script_name="${1:-}"
    if [[ -z "$script_name" ]]; then
        >&2 echo "Error: no script name provided"
        return "$WK_INVALID_ARGUMENT"
    fi
    shift

    wk_util_debug "[wk_util_callscript_recurse] Run '$script_name' for $*"

    local env_name
    for env_name in "$@"; do
        wk_util_debug "[wk_util_callscript_recurse] Entering '$env_name'"
        WK_HOME="$current_home" wk_util_callscript "$env_name" "$script_name"
        current_home="$current_home/$env_name"
    done
}


wk_usage() {
    echo "\
Usage:
$ wk <project-name>
$ wk <command> [options] <project-name>"
}


wk_set_cwd() {
    export WK_CWD="$1"
}


wk_get_cwd() {
    echo "$WK_CWD"
}


# Invoke wk under a parent project.
# 1 - the parent project; must exist as a project.
# @ - arguments to be passed to wk.
wk_as_parent() {
    echo "wk_as_parent is deprecated. You probably need to change"
    echo "your super command's implementation to:"
    echo "$1() { WK_HOME=\"$WK_HOME/$1\" wk \"\$@\"; }"
    local WK_HOME_ORIGINAL=$WK_HOME
    export WK_HOME="$WK_HOME_ORIGINAL/$1"
    shift
    wk "$@"
    export WK_HOME=$WK_HOME_ORIGINAL
}


# Make a project a "super" project.
# Injects a command ($2) to the project's setup script.
# 1 - the project to add a command to.
# 2 - the command name; must not contain special characters.
wk_make_super() {
    wk_util_debug "[wk_make_super] Args: $*"
    local PROJECT=$1
    local COMMAND=$2
    wk_util_debug "[wk_make_super] Project $PROJECT; command $COMMAND"
    if [[ -z "$COMMAND" || -z "$PROJECT" ]]; then
        echo "usage: wk_make_super <project> <command>"
        return "$WK_INVALID_SYNTAX"
    fi
    local DIR="$WK_HOME/$PROJECT"
    if [[ ! -d "$DIR" ]]; then
        echo "No such environment: [$PROJECT] (at [$DIR])"
        return $WK_NO_SUCH_ENV;
    fi

    if [[ $COMMAND == *" "* || $COMMAND == *"~"* || $COMMAND == *"."* ]]; then
        echo "Invalid command name: $COMMAND"
        return $WK_NO_SUCH_COMMAND;
    fi

    echo -e "# Invoke wk within this super-env.\n$COMMAND() { WK_HOME=\"$WK_HOME/$PROJECT\" wk \"\$@\"; }" >> "$DIR/setup.sh"
    echo -e "# Unset the command when leaving this super-env.\nunset $COMMAND" >> "$DIR/teardown.sh"
}


# Create a new wk project.
# First validates the directory and project name. If there is no creation
# script at `$WK_HOME/create.sh`, `wk_default_create` will be
# called with the validated project path and validated/auto-deduced project
# name. If the creation script does exist, it wil be called with those two
# arguments, followed by the current wk dir.
# 1 - the relative path of the project.
# 2 - the name of the project (optional; defaults to `basename $1`).
wk_create() {
    if [[ -z "$1" ]]; then
        echo "Path name required to create project."
        return "$WK_INVALID_SYNTAX"
    fi
    local DIR="$1"
    if [[ "$DIR" == "." || "$DIR" == ".." || $DIR == *"~"* ]]; then
        echo "Invalid path: $DIR"
        return "$WK_INVALID_ARGUMENT"
    fi
    mkdir -p "$DIR"
    local NAME
    NAME="${2:-$(basename "$DIR")}"
    local CREATE_SCRIPT="$WK_HOME/create.sh"
    if [[ -f "$CREATE_SCRIPT" ]]; then
        wk_util_debug "[wk_create] Found '$CREATE_SCRIPT'."
        "$CREATE_SCRIPT" "$DIR" "$NAME" "$WK_CWD"
    else
        wk_util_debug "[wk_create] No create.sh found. Using default."
        wk_default_create "$DIR" "$NAME"
    fi
}


# Create a new wk project.
# Assumes validated arguments. Creates a new directory for the project root and
# writes a simple setup.sh script.
wk_default_create() {
    local project_root="${1:-?}"
    local project_name="${2:-?}"
    if [[ "$project_name" == "?" ]] || [[ "$project_root" == "?" ]]; then
        >&2 echo "Usage:"
        >&2 echo "  wk_default_create <project-root> <project-name>"
        >&2 echo "Or:"
        >&2 echo "  wk_default_create - <project-name>"
        exit 1
    fi

    local wk_project_dir="$WK_HOME/$project_name"
    local setup_file="$wk_project_dir/setup.sh"
    wk_util_debug "[wk_default_create] Project root: '$project_root'"
    wk_util_debug "[wk_default_create] Project name: '$project_name'"
    wk_util_debug "[wk_default_create] Project dir: '$wk_project_dir'"
    wk_util_debug "[wk_default_create] Setup file: '$setup_file'"
    mkdir -p "$wk_project_dir"
    if [[ "$project_root" == "-" ]]; then
        touch "$setup_file"
    else
        echo -e "wk_set_cwd \"$project_root\"\ncd \"\$(wk_get_cwd)\"" \
            > "$WK_HOME/$2/setup.sh"
    fi
}


# Find the setup file for a project.
# 1 - the project name.
wk_edit() {
    DIRNAME="$WK_HOME/$1"
    if [[ ! -d "$DIRNAME" ]]; then
        echo "No such environment: [$1] (at [$DIRNAME])"
        return "$WK_NO_SUCH_ENV"
    fi
    editor "$DIRNAME/setup.sh"
}


# Link one env under another name.
# 1 - the existing project; must alread be a project.
# 2 - the alias; must not already be a project.
wk_link() {
    if [[ -z "$1" ]] || [[ -z "$2" ]]; then
        >&2 echo "Usage: wk_link <existing> <alias>"
        return "$WK_INVALID_ARGUMENT"
    fi
    local EXISTING="$WK_HOME/$1"
    if [[ ! -d "$EXISTING" ]]; then
        echo "No such environment: [$1] (at [$EXISTING])"
        return "$WK_NO_SUCH_ENV"
    fi
    local ALIAS="$WK_HOME/$2"
    if [[ -d "$ALIAS" ]]; then
        echo "wk: Cannot create alias '$ALIAS': project already exists"
        return "$WK_ENV_ALREADY_EXISTS"
    fi

    # Use relative paths so links don't break if $WK_HOME changes.
    # Does prevent child projects from creating links to siblings of their
    # parents, etc. Is this desirable?
    cd "$WK_HOME" || exit
    ln -s "$1" "$2"
    cd - || exit
}


# Move one env to another name.
# 1 - the env name; must exist.
# 2 - the new name; must not exist.
wk_move() {
    if [[ -z "$1" ]] || [[ -z "$2" ]]; then
        >&2 echo "usage: wk_move <existing> <alias>"
        return "$WK_INVALID_SYNTAX"
    fi
    local OLD_ID="$1"
    local NEW_ID="$2"
    local EXISTING="$WK_HOME/$OLD_ID"
    if [[ ! -d "$EXISTING" ]]; then
        >&2 echo "[wk_move] No such environment: [$OLD_ID] (at [$EXISTING])"
        return "$WK_NO_SUCH_ENV"
    fi
    local MOVETO="$WK_HOME/$NEW_ID"
    if [[ -d "$MOVETO" ]]; then
        >&2 echo "[wk_move] Cannot move to '$MOVETO': project already exists"
        return "$WK_ENV_ALREADY_EXISTS"
    fi
    mv "$EXISTING" "$MOVETO"
}


# List all projects.
wk_list() {
    wk_util_debug "[wk_list] ls $* $WK_HOME"
    ls "$@" "$WK_HOME"
}


# Call the setup script for an env.
#
#   wk_setup <env> [sub-envs...]
wk_setup() {
    wk_util_debug "[wk_setup] Setup $*"
    wk_util_callscript_recurse setup "$@"
}


# Call the teardown script.
# 1 - the env name.
wk_teardown() {
    local project="${1:-}"
    if [[ -z "$project" ]]; then
        >&2 echo "[wk_teardown] No project specified"
        return "$WK_INVALID_ARGUMENT"
    fi
    wk_util_debug "[wk_teardown] Leaving '$project'"
    shift
    wk_util_callscript "$project" teardown
}


# Main command.
wk() {
    # Get wk options
    OPTIND=1 # Reset in case getopts has already been used.
    while getopts "hv?:" option; do
      case "$option" in
        h)
          wk_usage
          return 0
          ;;
        -*)
          echo "Unrecognised switch [$1]"
          return "$WK_NO_SUCH_OPTION"
          ;;
        esac
    done
    shift $((OPTIND-1))

    local command="${1:-}"
    if [[ -z "$command" ]]; then
        wk_usage
        return "$WK_INVALID_SYNTAX"
    fi
    wk_util_debug "[wk] Command '$command'; args [$*]"

    # At this point, $@ is all the command arguments (inc. options).
    case "$command" in
        setup|on) shift; wk_setup "$@" ;;
        teardown|off) shift; wk_teardown "$@" ;;
        create|new) shift; wk_create "$@" ;;
        edit) shift; wk_edit "$@" ;;
        ln|link) shift; wk_link "$@" ;;
        mv|move) shift; wk_move "$@" ;;
        ls|list) shift; wk_list "$@" ;;
        super) shift; wk_make_super "$@" ;;

        *)
            wk_util_debug "[wk] Command '$command' not found: assuming env setup"
            wk_setup "$@"
            ;;
    esac
}
