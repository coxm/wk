doc_src_dir=doc/md
doc_sources=$(shell find $(doc_src_dir) -type f -wholename "*.md")

manpage_dir=doc/man
manpages=$(doc_sources:$(doc_src_dir)/%.md=$(manpage_dir)/%.1)

manpages_install_dir=/usr/local/share/man/man1


.PHONY: docs
docs: $(manpages)
	


.PHONY: install
install: docs
	mkdir -p $(manpages_install_dir)
	cp $(manpages) $(manpages_install_dir)/


$(manpage_dir)/%.1: $(doc_src_dir)/%.md
	mkdir -p $(manpage_dir)
	pandoc --from=markdown --to=man "$^" | \
		sed -e 's/^\.SH/.TH/' \
		> "$@"
