# WK "1" "2016-11-03" "ProjectSetup" "User Commands"

## NAME

wk - manage shell environments

## SYNOPSIS

```bash
wk <env> [subenv...]
wk <command> [arg...]
```

## DESCRIPTION

Create and manage shell environments. Functionality is split into a number of sub-commands, each with its own options.

## OPTIONS

`-h`

:   Display help text and exit.

## SYNTAX

Functionality is split up into several sub-commands, with syntax as follows.
Where possible, if the command is omitted it defaults to **setup**.

`wk new <project-dir> [name]`

:   Create a new environment with the specified root directory and name. If the
    name is omitted, it defaults to the base name of the directory.

`wk on <name>`

:   Set up an existing environment. Does not teardown any existing
    environments, making composition possible.

`wk off <name>`

:   Run the named environment's `teardown.sh` script.

`wk mv <name> <newname>`

:   Rename an environment.

`wk ls [option]...`

:   List existing environments. Accepts any options which can be passed to the
    *ls* command.

    See *ls*(1).

`wk super <name> <command>`

:   Make an existing environment a "super env", enabling it to contain child
    environments. Management of these child environments will be possible using
    a newly created function named as specified. The **wk** command will still
    be available for global environment management while using super envs.


## EXAMPLES

Simple `.bashrc` setup:

```bash
export WK_HOME=$HOME/wk_home
source /path/to/wk.sh
```

Create an env:

```bash
wk new $HOME/my-project myenv
cat "echo 'hello'" >> "$WK_HOME/myenv/setup.sh"
cat "echo 'goodbye'" >> "$WK_HOME/myenv/teardown.sh"
wk myenv  # Echoes 'hello'.
pwd  # $HOME/my-project
wk teardown myenv  # Echoes 'goodbye'.
```

Create a super env:

```bash
mkdir -p $HOME/parent/child
wk new $HOME/parent  # Creates a 'parent' env rooted at $HOME/parent.
wk super parent pwk  # Creates a new command (pwk) to manage the super-env.
pwk new $HOME/parent/child child  # Creates a sub-env called 'child'.
pwk child  # Work on the child env.

# Alternatively we can descend straight into the child project:
wk parent child
```


## RETURN VALUES

`0 ($WK_SUCCESS)`

:   Success.

`1 ($WK_NO_SUCH_COMMAND)`

:   An invalid command was provided.

`2 ($WK_NO_SUCH_OPTION)`

:   An invalid option was provided.

`4 ($WK_NO_SUCH_SCRIPT)`

:   Attempted to run a script which does not exist.

`8 ($WK_NO_SUCH_ENV)`

:   An environment required for some operation does not exist.

`16 ($WK_INVALID_SYNTAX)`

:   Invalid syntax.

`32 ($WK_INVALID_ARGUMENT)`

:   An invalid argument was provided.

`64 ($WK_ENV_ALREADY_EXISTS)`

:   An environment already exists where **wk** expected to find none.



## SECURITY CONSIDERATIONS

Several **wk** commands execute scripts under the *WK_HOME* directory. As such,
this directory should be appropriately protected, and it is advised to never to
use **wk** with commands like **su**/**sudo** or as any user with dangerous
permissions (e.g. root).

## BUGS

Please report bugs via whichever service the repository is currently hosted.
Pull requests are also welcome!

## AUTHORS

This manual page was written by Michael Cox <coxm@noreply.users.github.com>.

## LICENSE

Permission is granted to copy, distribute and/or modify this document under the
terms of the GNU General Public License, version 3 or any later version
published by the Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License can be
found in /usr/share/common-licenses/GPL.

## COPYRIGHT

Copyright © 2016 Michael Cox
